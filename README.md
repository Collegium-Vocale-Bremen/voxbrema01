# Vox Brema

## noch offen

* Logo/Brand mit korrekter Schrift  -> Nina Rathje <rathje.ninamelissa@gmail.com>
* Vielleicht insgesamt eine andere Schrift wählen?
* Die drei Legal Seiten fertigstellen. Aktuell nur "geklaute" Platzhalter.
* Bessere Bilder für die HOME Seite.
* HOME - Hallo - Abstand zwischen Bild und Tect ist zu groß
* HOME - Termine: hinzufügen
* KinderchorimViertel  

## Struktur

### Damit fangen wir an
* Landing Page (Home)
* Leitung (über Kamila)
* Chöre
** Kinderchor
** Ausbildungschor (Chorschule für Erwachsene)
** Projektchor
* Kontakt

### Das kommt dann ggf. später noch dazu
* Termine (ggf. direkt auf der Landing Page)
* Presse
* Galerie
* Downloads 
* Google Map ID und Karten

### Das muss leider sein
* Datenshutzerklärung
* Impressum
* Disclaimer
* 

### Hinweis
Die alte Seite "Kinderchor im Viertel" wird hierhin weitergeleitet.
Ggf. Verlinkung auf die anderen Bremer Chöre aus Kamilas Umfeld

15.09.2019: www.kinderchorimviertel.de bei Strato bestellt: 2,50 + 12 x 1,50 Euro
EInfach umleiten oder eine kleine statische Seite mit dem Hinweis, dass das jetzt unter VoxBrema läuft?

### Wichtige Merkmale
1. Ein Name, Drei Gruppen, eine Musikalische Familie
1. Vox Brema ist kein 'Singverein', sondern steht für Qualität.
1. Vox Brema ist bunt und offen für alle
1. Besonders im Projektchor haben Laien die Möglichkeit, gemeinsam mit externen Coaches zu arbeiten und auf der Bühne mit Profis zu singen.
1. Kamila mag Blautöne ;)
1. Verlinkung auf KAmilas Facebook Seite

## Technisches 

### Building a hugo site from scratch (using a bootstrap theme)

https://willschenk.com/articles/2018/building-a-hugo-site/

    hugo server --watch --verbose --buildDrafts --cleanDestinationDir --disableFastRender
    
    hugo -D serve --buildFuture

    -> localhost:1313

 This is the best tutorial how to create static "landing pages" on Hugo: https://discuss.gohugo.io/t/creating-static-content-that-uses-partials/265/19?u=royston

Basically, you create .md in /content/ with type: "page" in front matter, then create custom layout for it, for example layout: "simple-static" in front matter, then create the layout template in themes/<name>/layouts/page/, for example, simple-static.html. Then, use all partials as usual, and call content from original .md file using {{ .Content }}.

### Bootstrap Theme

Free Bootstrap Theme from https://colorlib.com/wp/cat/bootstrap/

Musica https://colorlib.com/wp/template/musica/

Licence: Creative Commons Attribution 3.0 License (CC BY 3.0) (= footer credits must remain in place)

Images from Unsplash

Legal Attribution for Free Templates

Colorlib provides professionally designed mobile-friendly website templates. When you download or use our templates without paying, it will attribute the following conditions. Which means:

You are allowed to

* use templates for personal and business usage.
* customize templates however you like.
* convert/port HTML templates for use for any CMS
* share and distribute them under Colorlib brand name.

Limitations for FREE versions

* You are not allowed to remove the copyright credit from the footer.
* You are not allowed to sell, resale, rent, and lease our template.
* We don’t provide any technical support for free HTML templates.


### Colorlib Copyright credit

    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

### Ähnliche Themes

https://themewagon.com/themes/one-music-free-bootstrap-4-html5-music-website-template/  (wer klaut da von wem?)

Mal auf github oder gilab nach bootstrap themes suchen...

## Datenschutzerklärung, Disclaimer, Impressum
Quellverweis: eRecht24 (www.e-recht24.de), das Portal zum Internetrecht von Rechtsanwalt Sören Siebert