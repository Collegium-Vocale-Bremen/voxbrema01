---
title: "Leitung"
subtitle: "Kamila stellt sich vor"
description: "Die Vox Brema Leitung stellt sich vor"
type: "page"
color: "#FFDE59"
draft: false
---

![Kamila](/img/bg-img/kamila-welcome-1.jpg)

Unsere Leiterin **Kamila Dunajska** erzählt, dass sie singen wollte, seit sie denken kann. Laut ihren Eltern hat sie schon gesungen, bevor sie noch sprechen konnte. Schon im Kindergartenalter gewann sie in ihrer Heimatstadt, dem polnischen Danzig, den ersten Gesangswettbewerb für Kinder.  
Während der gesamten Grundschulzeit besuchte sie eine renommierte staatliche Musikschule, wo sie im Chor sang und Klavier- und Querflötenunterricht nahm. Daher konnte sie schon als Teenagerin ihren ersten musikalischen Nebenjob als Flötistin in einer professionellen Blaskapelle übernehmen.

Gleichzeitig mit dem Abitur erhielt sie auch den Berufsdiplom für Gesang an dem Musikgymnasium in Danzig. Danach folgte das Studium, zuerst an der Musikakademie Danzig mit der Studienrichtung Gesang und Schauspiel, und anschließend an der Hochschule für Künste in Bremen mit der Studienrichtung Gesang. Parallel zum Studium wurde sie außerdem in Hannover von Professorin Charlotte Lehmann betreut.

Nachdem sie ihre absolute Leidenschaft für das Chor- und Orchesterdirigieren entdeckt hatte, begann sie, sich auch in dieser Richtung weiterzubilden.
Sie absolvierte ein Weiterbildungsstudium Chorleitung an der Hochschule für Musik Hanns Eisler in Berlin bei Prof. Jörg Peter Weigle und nimmt ständig an verschiedenen Meisterkursen und Fortbildungen für Dirigenten statt.

Sie ist die ganze Zeit sowohl solistisch als Sopranistin als auch als Gesangslehrerin tätig. Schwerpunkt ihrer gesangspädagogischen Arbeit ist, neben der Einzelbetreuung in ihrem Gesangsstudio im Viertel, die stimmbildnerische Arbeit mit Chören aller Altersstufen und Arten.
In Deutschland wie in Polen hält sie Stimmbildungsseminare (für Choristen) ab und leitet Workshops für Bühnenpräsenz. Außerdem bietet sie für andere ChorleiterInnen aus Bremen und Niedersachsen Fortbildungskurse in Dirigiertechnik und Probenmethodik an.

Kamila Dunajska leitet neben **"Vox Brema"** noch drei weitere Chöre,den A-Cappella-Jazzchor **"Voice in Fusion"**, den Kammerchor **"Collegium Vocale Bremen"** und den **"Coro di Solisti"**-Konzertchor von professionellen SängerInnen, die zur Mitarbeit in Konzertprojekten eingeladen werden und in ausgewählter Besetzung für Konzerte zusammenkommen.