---
title: "Impressum"
type: "page"
color: "white"
draft: false
---

### Angaben gemäß § 5 TMG
Vox Brema 

### Vertreten durch
Kamila Dunajska

### Kontakt
Telefon:  
E-Mail:  

### Umsatzsteuer
Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz: DE[...]

### Angaben zur Berufshaftpflichtversicherung
[...]

### Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:
Christoph

### Streitschlichtung
Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit: https://ec.europa.eu/consumers/odr.
Unsere E-Mail-Adresse finden Sie im Impressum.

Quellverweis: eRecht24 (www.e-recht24.de), das Portal zum Internetrecht von Rechtsanwalt Sören Siebert