---
title: "Projektchor 2020 - Vorklasse"
subtitle: "Von feiner Kammermusik bis hin zu den schönsten Opernchören"
date: 2019-10-23T19:45:00+01:00
location: "Gesamtschule Mitte Standort Brokstraße"
image: "projektchor.jpg"
draft: false
address: "Gesamtschule Mitte - Standort Brokstraße, Sielwall 86, 28203 Bremen"
latitude: 53.069220
longitude: 8.824820
zoom: 14
---

„Va pensiero“ aus Nabucco, „Si ridesta in ciel” aus La Traviata und “Welch' Festes Glanz” aus Eugen Onegin sind nur einige der Stücke, die unser Projektchor 2019 während des Konzerts 28. September 2019 in der Bremer Liebfrauenkirche gesungen hat. 

Genauso ambitioniert und erfolgreich soll es daher im nächsten Jahr mit einem neuen Projektchor weitergehen. Vielleicht erstürmen wir sogar ganz neue musikalische Höhen. Der Projektchor ist ein Projekt der musikalischen Klassik. In seinem Repertoire werden sich die schönsten Hits der Chormusik finden.

Das nächste Projekt wird von Januar bis Ende September 2020 dauern. Wir werden **ab dem 8. Januar 14-tägig jeweils mittwochs von 19:45 Uhr bis 21:45 Uhr** proben (außer während der Schulferien).

In der Zwischenzeit gibt es die **Projektchor-Vorklasse** mit insgesamt vier Proben für die, die nicht so lange pausieren möchten; die mehr Zeit möchten, um sich in die neuen Stücke einzulesen; die in kleiner Gruppe intensiv am klassischen Klang arbeiten möchten; und natürlich auch für die, die mal Reinschnuppern möchten, bevor sie sich für das neue Projekt entscheiden. Der Teilnehmerbetrag liegt bei **54,00 Euro**. Die Proben werden jeweils mittwoches am **23.10., 06.11., 27.11. und 11.12. um 19:45 Uhr** in der Gesamtschule Mitte stattfinden.

**Anmeldung** für die Vorklasse 2019 und/oder den Projektchor 2020 ab sofort bei [Kamila Dunajska](../../contact).