---
title: "Weihnachtsfeier"
subtitle: "Die große Weihnachtsfeier des Gesangsstudios im Viertel!!!"
date: 2019-12-13T19:30:00+01:00
location: "Kulturzentrum Lagerhaus"
image: "lagerhaus.jpg"
draft: false
address: "Kulturzentrum Lagerhaus, Schildstraße 12-19, 28203 Bremen"
latitude: 53.0720362
longitude: 8.82229525
zoom: 14
---

#### Liebe Freundinnen und Freunde, liebe Chories,

es findet auch dieses jahr wieder statt:

**Große Weihnachtsfeier des Gesangsstudios im Viertel!!!**

Am **Freitag, den 13. Dezember 2019, ab 20:00 Uhr**     

im Lagerhaus Schildstraße (1. Etage, KIOTO) 

Einlass: 19:30 Uhr

Es erwartet uns: Weihnachtliche Jam-Session, Karaoke und Disco mit DJ Jenny. 

Ich freue mich auf Euch!

Mit musikalischen Grüßen,  
Kamila