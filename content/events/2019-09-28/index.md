---
title: "Klassik groovt - Bremen lauscht"
subtitle: "Von feiner Kammermusik bis hin zu den schönsten Opernchören"
date: 2019-09-28T20:00:00+01:00
location: "Unsere Lieben Frauen Kirche"
image: "bild.jpg"
draft: false
address: "Unser Lieben Frauen Kirchhof 27, 28195 Bremen"
latitude: 53.076470
longitude: 8.807240
zoom: 14
---

Wohl noch nie im Leben habe ich mich auf ein Chorkonzert so gefreut wie auf dieses. Dafür gibt es einen bestimmten Grund.

Mir liegt viel daran, Ihnen heute Abend die ganze Vielseitigkeit des klassischen Gesangs vorzustellen; die verschiedenen Musikepochen und -stile, die ganze Bandbreite der Klangfarben und musikalischen Eigenarten. Deswegen spannt sich in diesem Konzert der Bogen von feiner, geistlicher Kammermusik über die schönsten Opernchöre bis zur spritzigen Operette.
  