---
title: "Chorschule für Kinder"
subtitle: "Professionelle, kindgerechte Stimmbildung"
description: "Singbegeisterte Kinder ab fünf Jahren sind herzlich willkommen bei unserer Vox Brema Chorschule für Kinder aus ganz Bremen."
type: "page"
color: "#E683C3"
backgroundimage: "img/bg-img/Chorschule-Kinder2.jpg"
draft: false
---

## Singbegeisterte Kinder ab fünf Jahren sind herzlich willkommen bei unserer Chorschule für Kinder aus ganz Bremen.

Das Besondere an unserem Chor ist die professionelle, kindgerechte Stimmbildung und die damit verbundenen starken selbstbewussten Stimmen der jungen Sängerinnen und Sänger.

Bei den Proben
* können die Kinder mit ihrer Stimme experimentieren
* erlernen die Kinder eine kindgerechte Stimmbildung in altersgerechter Tonhöhe
* entwickeln und verstärken die Kinder ihr Rhythmusgefühl

Kamila Dunajska und die Kinder werden gemeinsam fantasievolle Klanggeschichten mit Bewegungselementen entstehen lassen. Ganz spielerisch werden die Kinder an musiktheoretische Grundlagen herangeführt.

Mit der Zeit üben Kinder hier ein anspruchsvolleres Repertoire ein. Mit bemerkenswerter Leichtigkeit singen sie bereits mehrstimmige A-cappella-Arrangements aus Film- und Popmusik. Auch erste schwierige Gesangsstücke der Klassik werden mit der erlernten Gesangstechnik bewältigt.

## Chorproben
**Chorproben** sind immer dienstags in der **Gesamtschule Mitte, Standort Brokstraße, Sielwall 86, 28203 Bremen**, im Musikraum, Zugang vom hinteren Schulhof aus. Die Probe des Kinderchores beginnt um **16.15 Uhr**.