---
title: "Ausbildungschor"
subtitle: "Der einzige „Ausbildungschor“ dieser Art in Bremen!"
description: "Der Vox Brema Ausbildungchor - der einzige „Ausbildungschor“ dieser Art in Bremen"
type: "page"
color: "#8BCCC7"
backgroundimage: "img/bg-img/ausbildungschor2.jpg"
draft: false
---

_Ausbildungschor_ heißt, wir singen nicht nur Lieder mehrstimmig, sondern jede Probe beinhaltet vor allem:

### Chorische Ausbildung
Chorische Ausbildung, d.h. bewusste Intonation und das aufeinander Hören sowie Singen vom Blatt ohne Stress, Rhythmik inkl. Rhythmussprache und Mentales Coaching vor und während eines Auftrittes – d. h. Umgang mit Lampenfieber: was ist zu tun, wenn die Muskeln sich verspannen und die Stimme nicht mehr locker und frei läuft?

### Stimmbildung
Die Stimmbildung ist körperzentriert und basiert auf mentalem Training in Verbindung mit Schauspieltechniken.

### Musikalische Bearbeitung des Repertoires
Die musikalische Bearbeitung des Repertoires, das Querbeet durch alle Stilrichtungen wie Pop, Rock, Jazz, Klassik, etc. und in verschiedenen Sprachen sein wird, die alle drei miteinander verbunden werden!

### Chorproben
Immer **mittwochs** von **17.30 – 19.30 Uhr** im **Ostertor, 28201 Bremen**